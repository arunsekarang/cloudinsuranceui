import { Component, OnDestroy, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { navItems } from '../../_nav';
import { Router } from '@angular/router';


@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html'
})
export class DefaultLayoutComponent implements OnDestroy {
  public navItems = navItems;
  public sidebarMinimized = true;
  private changes: MutationObserver;
  public element: HTMLElement;
  public xtoggle: Boolean = false;

  constructor(public router: Router, @Inject(DOCUMENT) _document?: any) {

    this.changes = new MutationObserver((mutations) => {
      this.sidebarMinimized = _document.body.classList.contains('sidebar-minimized');
    });
    this.element = _document.body;
    this.changes.observe(<Element>this.element, {
      attributes: true,
      attributeFilter: ['class']
    });

    //this.leftSideToggle();

  }

  ngOnDestroy(): void {
    this.changes.disconnect();
  }

  leftSideToggle(event) {
    var ua = window.navigator.userAgent;
    if (ua.indexOf("MSIE ") != -1 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
      if (event.srcElement.className == 'navbar-toggler d-md-down-none') {
        this.xtoggle = !this.xtoggle;
      }
    }
    if (event.srcElement.className == 'navbar-toggler-icon') {
      this.xtoggle = !this.xtoggle;
    }
  }

  logout() {
    localStorage.removeItem('userData');
    this.router.navigate(['/login']);
  }

}
