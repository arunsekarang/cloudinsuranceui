import { Injectable } from '@angular/core';
import { RequestOptions, Http, HttpModule, Response } from '@angular/http';
import { environment } from '../../../environments/environment';
import { Observable, Subscriber } from 'rxjs';
import { tap, map, filter } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private http: Http, public httpClient: HttpClient) { }

  /**
   * Service for Dashboard Commission part
   */
  commissionCalculation() {
    const commissionData = {
      'agentId': '08910042'
    };

    const userData = JSON.parse(localStorage.getItem('userData'));
    console.log(userData.authToken);

    const getHeaders = new HttpHeaders();
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        // 'X-Auth-Token': 'EAM4Y2LVR2LCM44IA2T47GJ9AI18ON'
       // 'X-Auth-Token': userData.authToken
      })
    };

    return this.httpClient.post(environment.ENVIRONMENT.baseURL + 'Payments/agentBalance', commissionData,
      httpOptions);
  }

  /**
   * Service for Dashboard Performance Graph Part
   */
  performanceGraph(params) {
    const commissionData = {
      'agentId': '90000005',
      'productionName': 'Group APE Ack',
      'periodMonth':  params.selectMonth.toString(),
      'periodYear': params.selectYear.toString()
    };

    const getHeaders = new HttpHeaders();
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
        // 'X-Auth-Token': 'EAM4Y2LVR2LCM44IA2T47GJ9AI18ON'
      })
    };

    // return this.httpClient.post(environment.ENVIRONMENT.baseURL + 'Performance/getAchievementRatio', commissionData,
    // httpOptions).subscribe(
    //     (response: Response) => (response.json())
    // );

    return this.httpClient.post(environment.ENVIRONMENT.baseURL + 'Performance/getAchievementRatio', commissionData,
      httpOptions);
  }


  /**
   * Service for Dashboard Team Performance Table List
   */
  teamPerformanceGraph(params) {
    const teamPerformanceData = {
      'agentId': '90000005',
      'productionName': 'Group APE Ack',
      'periodMonthFrom':  params.periodMonthFrom.toString(),
      'periodYearFrom': params.periodYearFrom.toString()
    };

    const getHeaders = new HttpHeaders();
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
        // 'X-Auth-Token': 'EAM4Y2LVR2LCM44IA2T47GJ9AI18ON'
      })
    };

    return this.httpClient.post(environment.ENVIRONMENT.baseURL + 'Performance/getTopProducers', teamPerformanceData,
      httpOptions);
  }


  /**
   * Service for Dashboard Team Performance Table List
   */
  performanceProductionGraph(params) {
    const performanceProductionData = {
      'agentId': '90000005',
      'productionName': 'Group APE Ack',
      'periodMonth':  params.periodMonthFrom.toString(),
      'periodYear': params.periodYearFrom.toString()
    };

    const getHeaders = new HttpHeaders();
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
        // 'X-Auth-Token': 'EAM4Y2LVR2LCM44IA2T47GJ9AI18ON'
      })
    };

    return this.httpClient.post(environment.ENVIRONMENT.baseURL + 'Performance/agentIndividualProduction', performanceProductionData,
      httpOptions);
  }


   /**
   * Service for Dashboard Contest Details
   */
  contestDetails() {
    const performanceProductionData = {
      'agentId': '80000981',
      'evaluationName': 'Agency Quarterly Bonus'
    };

    const getHeaders = new HttpHeaders();
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
        // 'X-Auth-Token': 'EAM4Y2LVR2LCM44IA2T47GJ9AI18ON'
      })
    };

    return this.httpClient.post(environment.ENVIRONMENT.baseURL + 'Compensation/getContestData', performanceProductionData,
      httpOptions);
  }


  /**
   * Service for Dashboard Agent Application Status Details
   */
  agentApplicationStatus() {
    const agentApplicationData = {
      'agentId': '80000087'
    };

    const getHeaders = new HttpHeaders();
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
        // 'X-Auth-Token': 'EAM4Y2LVR2LCM44IA2T47GJ9AI18ON'
      })
    };

    return this.httpClient.post(environment.ENVIRONMENT.baseURL + 'AgentInforamtion/agentApplicationStatus', agentApplicationData,
      httpOptions);
  }
}
