import { Injectable } from '@angular/core';
import {  Http, HttpModule, Response } from '@angular/http';
import { environment } from '../../../environments/environment';
import { Observable, Subscriber } from 'rxjs';
import { tap, map, filter } from 'rxjs/operators';
import { HttpClientModule } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: Http) {
  }

  /**
   * Service for login
   */
  login(username, password) {
    const loginData = {
      'userId' : username,
      'password' : password,
      'userType' : 'ISO'
    };
    return this.http.post(environment.ENVIRONMENT.baseURL + 'login/authenticate', loginData).pipe(map(
      (response: Response) => (response.json())
    ));
  }
}
