interface NavAttributes {
  [propName: string]: any;
}
interface NavWrapper {
  attributes: NavAttributes;
  element: string;
}
interface NavBadge {
  text: string;
  variant: string;
}
interface NavLabel {
  class?: string;
  variant: string;
}

export interface NavData {
  name?: string;
  url?: string;
  icon?: string;
  badge?: NavBadge;
  title?: boolean;
  children?: NavData[];
  variant?: string;
  attributes?: NavAttributes;
  divider?: boolean;
  class?: string;
  label?: NavLabel;
  wrapper?: NavWrapper;
}

export const navItems: NavData[] = [
  {
    name: 'Dashboard',
    url: '/dashboard',
  },


  {
    name: 'Production',
    url: '/dashboard',
    children: [
      {
        name: 'Production Details',
        url: '/dashboard',
      },
      {
        name: 'Computation Details',
        url: '/dashboard',
      },
      
    ]
  },
  {
    name: 'Contest',
    url: '',
    children: [
      {
        name: 'Evaluation Details',
        url: '/dashboard',
      },
      {
        name: 'Production Details',
        url: '/dashboard',
      },
    ]
  },
  {
    name: 'Balance',
    url: '/dashboard',
  },
  {
    name: 'My Team',
    url: '/dashboard',
  },

];
