import { Component, OnInit, Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from '../../restful-service/login-service/login.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  mobileForm: FormGroup;
  otpForm: FormGroup;
  public displayLogin: Boolean = true;
  public displayMobile: Boolean = false;
  public displayOTP: Boolean = false;
  constructor(private formBuilder: FormBuilder, private router: Router, private loginService: LoginService,
    private toastr: ToastrService) {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
    this.mobileForm = this.formBuilder.group({
      mobileno: ['', Validators.required]
    });
    this.otpForm = this.formBuilder.group({
      mobileotp: ['', Validators.required]
    });
  }

  ngOnInit() {
  }

  loginSubmit() {
    if ((this.loginForm.value.username !== '' && this.loginForm.value.password !== '')) {
      this.loginService.login(this.loginForm.value.username, this.loginForm.value.password).subscribe(
        data => {
          if (data.status === 'Success') {
            this.toastr.success('Login Success', '', {
              timeOut: 3000
            });
            localStorage.setItem('userData', JSON.stringify(data.data[0]));
            this.router.navigate(['dashboard']);
          } else {
            this.toastr.error('Login Failed', data.message, {
              timeOut: 3000
            });
          }
        }, error => {

        });
    }
  }

  toggleForm(showLogin, showMobile, showOtp) {
    this.displayLogin = showLogin;
    this.displayMobile = showMobile;
    this.displayOTP = showOtp;
  }


}
