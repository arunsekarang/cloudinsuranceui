import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { DashboardService } from '../../restful-service/dashboard-service/dashboard.service';
import { CommonModule, DatePipe } from '@angular/common';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { map } from 'rxjs/operators';

@Component({
  templateUrl: 'dashboard.component.html'
})
export class DashboardComponent implements OnInit {

  radioModel: String = 'y';
  commissionValue = 0;
  compensationValue = 0;
  onHoldValue = 0;
  payableValue = 0;
  otherValue = 0;
  responseData: any;
  GroupAPEAck: any;
  GroupAPEAck1: any;
  GroupAPEAck2: any;
  GroupAPEAck3: any;
  GroupAPEAck4: any;
  displayLoader: Boolean = false;
  performanceProductionSelectProduct: any;
  teamPerformanceSelectProduct: any;
  
  public monthList: Array<any> = ['', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep',
    'Oct', 'Nov', 'Dec'];

  /* Common Variable Declaration */
    public performanceYear: any[] = [];
    public datenow = new Date();
    public targetValueLineChart: Array<number> = [];
    public actualValueLineChart: Array<number> = [];
    public performanceLineChart: Array<any> = [
      {
        data: this.targetValueLineChart,
        label: 'Target'
      },
      {
        data: this.actualValueLineChart,
        label: 'Actual'
      }
    ];
    public performanceChartType = 'line';

  /** Performance Graph Section Variable Starts Here */
    /** Performance Graph Filter Section Variable Declaration */
    performanceRadioModel: String = 'y';
    performanceSelectYear: Number;
    performanceSelectMonth: String = '';

    /* tslint:disable:max-line-length */
    /* Variable Declarations for Performance Line Chart */
    public performanceLineChartLabels: Array<any> = [];
    public performanceLineChartOptions: any = {
      tooltips: {
        enabled: false,
        custom: CustomTooltips,
        intersect: true,
        mode: 'index',
        position: 'nearest',
        callbacks: {
          labelColor: function (tooltipItem, chart) {
            return { backgroundColor: chart.data.datasets[tooltipItem.datasetIndex].borderColor };
          }
        }
      },
      responsive: true,
      maintainAspectRatio: false,
      scales: {
        xAxes: [{
          gridLines: {
            drawOnChartArea: false,
          },
          ticks: {
            callback: function (value: any) {
              // return value.charAt(0);
              return value;
            }
          }
        }],
        yAxes: [{
          ticks: {
            beginAtZero: true,
            maxTicksLimit: 5,
            stepSize: Math.ceil(250 / 5)
          }
        }]
      },
      elements: {
        line: {
          borderWidth: 3,
          tension: 0
        },
        point: {
          radius: 6,
          hitRadius: 10,
          hoverRadius: 4,
          hoverBorderWidth: 3,
        }
      },
      legend: {
        display: false
      }
    };
    public performanceLineChartColours: Array<any> = [
      { // brandInfo
        backgroundColor: 'transparent',
        borderColor: '#6a87ff',
        pointHoverBackgroundColor: '#fff'
      },
      { // brandSuccess
        backgroundColor: 'transparent',
        borderColor: '#83cd83',
        pointHoverBackgroundColor: '#fff'
      },
      { // brandDanger
        backgroundColor: 'transparent',
        borderColor: getStyle('--danger'),
        pointHoverBackgroundColor: '#fff',
        borderWidth: 1,
        borderDash: [8, 5]
      }
    ];
    public performanceLineChartLegend = true;

    /* Variable Declarations for Performance Bar Chart */
    public performanceBarChartData: any[] = [{ data: [], label: '' }, { data: [], label: '' }];
    public performanceBarChartLabels: string[] = [];
    public performanceBarChartOptions: any = {
      scaleShowVerticalLines: false,
      responsive: true,
      legend: {
        display: true,
        position: 'bottom'
      }
    };
    public performanceBarChartLegend = true;
    public performanceBarChartType = 'bar';
    public performanceBarChartColours: Array<any> = [
      { // brandInfo
        backgroundColor: '#03c4b3',
        borderColor: getStyle('--info'),
        pointHoverBackgroundColor: '#fff'
      },
      { // brandSuccess
        backgroundColor: '#878787',
        borderColor: getStyle('--success'),
        pointHoverBackgroundColor: '#fff'
      }
    ];

    /* Variable Declarations for Performance Doughnut Chart */
    public performanceDoughnutChartOptions: any = {
      scaleShowVerticalLines: false,
      responsive: true,
      legend: {
        display: true,
        position: 'bottom'
      },
      styling: {
        borderWidth: 100
      },
    };
    public performanceDoughnutChartDataBottom: number[] = [];
    public performanceDoughnutChartLabelsBottom: string[] = ['Target', 'Actual'];
    public performanceDoughnutChartTypeBottom = 'doughnut';
    public performanceDoughnutChartColor: Array<any> = [
      { // brandInfo
        backgroundColor: [
          '#c83c63',
          '#03c4b3'
        ]
      }
    ];
  /** Performance Graph Section Variable Ends Here */


  /** Team Performance Graph Section Variable Starts Here */
    /** Team Performance Graph Filter Section Variable Declaration */
    teamPerformanceRadioModel: String = 'y';
    teamPerformanceSelectYear: Number;
    teamPerformanceSelectMonth: String = '';
    teamPerformanceResultData = '';
  /** Team Performance Graph Filter Section Variable Ends Here */


  /** Performance Graph Right Side Section Variable Starts Here */
    /** Performance Graph Filter Section Variable Declaration */
    performanceProductionSelectYear: Number;
    performanceProductionSelectMonth: String = '';

    public performanceProductionBarChartOptions: any = {
      maintainAspectRatio: false,
      scaleShowVerticalLines: false,
      responsive: true,
      legend: {
        display: true,
        position: 'bottom'
      },

      scales: {
        xAxes: [{
          gridLines: {
            display: false,
            color: '#fff',
            zeroLineColor: '#fff',
            zeroLineWidth: 0
          }
        }],
        yAxes: [{
          gridLines: {
            display: false,
            color: '#fff',
            zeroLineColor: '#fff',
            zeroLineWidth: 0
          }
        }]
      },
    };
    public performanceProductionBarChartLabels: string[] = ['2006', '2007', '2008'];
    public performanceProductionBarChartColours: Array<any> = [
      {
        backgroundColor: '#03c4b3',
        borderColor: getStyle('--info'),
        pointHoverBackgroundColor: '#fff'
      },
      {
        backgroundColor: '#878787',
        borderColor: getStyle('--success'),
        pointHoverBackgroundColor: '#fff'
      }
    ];
    public performanceProductionBarChartType = 'horizontalBar';
    public performanceProductionBarChartLegend = true;

    public performanceProductionBarChartData: any[] = [
      { data: [65, 59, 80], label: 'Series A' },
      { data: [68, 48, 60], label: 'Series B' }
    ];
  /** Performance Graph Right Side Section Variable Ends Here */


  /** Contest Details Variable Starts Here */
    /** Contest Details Variable Declaration */
    public contestDetailEvaluationData: any[] = [];
    public contestDetailEvaluationDoughnutChartOptions: any = {
      scaleShowVerticalLines: false,
      responsive: true,
      tooltips: {
        enabled: false
      },
      legend: {
        display: false
      },
      styling: {
        borderWidth: 100
      },
      cutoutPercentage: 70,
      elements: {
        center: {
          text: '90%',
          color: '#FF6384', // Default is #000000
          fontStyle: 'Arial', // Default is Arial
          sidePadding: 20 // Defualt is 20 (as a percentage)
        }
      }
    };
    public contestDetailEvaluationDoughnutChartData: any[] = [{
      data: [320, 100]
    }];
    public contestDetailEvaluationDoughnutChartType = 'doughnut';
    public contestDetailEvaluationDoughnutChartColor: Array<any> = [
      { // brandInfo
        backgroundColor: [
          '#ececec',
          '#c83c63'
        ]
      }
    ];
  /** Contest Details Variable Ends Here */


  /** Contest Details Variable Starts Here */
    /** Contest Details Variable Declaration */
    public agentApplicationChartOptions: any = {
      scaleShowVerticalLines: false,
      responsive: true,
      legend: {
        display: true,
        position: 'bottom'
      },
      styling: {
        borderWidth: 100
      },
      cutoutPercentage: 70
    };
    public agentApplicationLabels: string[] = [];
    public agentApplicationData: number[] = [];
    public agentApplicationType = 'doughnut';
    public agentApplicationChartColor: Array<any> = [
      { // brandInfo
        backgroundColor: [
          '#500033', '#c83c63', '#ffb14c', '#3598db', '#c9579f', '#03c4b3', '#878787', '#1c2c3c'
        ]
      }
    ];
  /** Contest Details Variable Ends Here */

  public random(min: number, max: number) {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }

  constructor(private router: Router, private dashboardService: DashboardService, public httpClient: HttpClient,
    private datePipe: DatePipe) {
    /** For Display List Of Years in performance year Filter DropDown in Chart Section */
    this.performanceYear.push(this.datenow.getFullYear());
    const currentYear = this.datenow.getFullYear();
    for (let i = 1; i <= 10; i++) {
      this.performanceYear.push(currentYear - i);
    }
    const currentMonthNumber = this.datenow.getMonth() + 1;

    /** Performance Graph Section Variable Starts Here */
    this.performanceSelectYear = currentYear;
    this.performanceSelectMonth = this.monthList[currentMonthNumber];

    /** Performance Production Graph Section Variable Starts Here */
    this.performanceProductionSelectYear = currentYear;
    this.performanceProductionSelectMonth = this.monthList[currentMonthNumber];

    /** Team Performance Graph Section Variable Starts Here */
    this.teamPerformanceSelectYear = currentYear;
    this.teamPerformanceSelectMonth = this.monthList[currentMonthNumber];

    this.commissionCalculation();
    this.performanceGraph(this.performanceRadioModel, this.performanceSelectYear, currentMonthNumber);
    this.teamPerformanceGraph(currentMonthNumber, this.teamPerformanceSelectYear);
    this.performanceProductionGraph(currentMonthNumber, this.teamPerformanceSelectYear);
    this.contestDetails();
    this.agentApplicationStatus();
  }

  ngOnInit(): void {
  }

  /**
   * Function for Dashboard Commission part
   */
  commissionCalculation() {
    this.displayLoader = true;
    this.dashboardService.commissionCalculation().subscribe(
      data => {
        this.displayLoader = false;
        if (data['status'] === 'Success') {
          data['data'].forEach(element => {
            // for Payable
            this.payableValue += parseInt(element.grossPayableValue, 0);

            // for Commission
            if (element.definitionType === 'Commission') {
              this.commissionValue = element.grossPayableValue;
            }

            // for Compensation
            if (element.definitionType === 'Compensation') {
              this.compensationValue = element.grossPayableValue;
            }

            // for Others
            if (element.definitionType === 'Others') {
              this.otherValue = element.grossPayableValue;
            }

            // for On Hold
            if (element.holdFl === 'Yes') {
              this.onHoldValue = element.grossPayableValue;
            }
          });
        }
      }, error => {
        this.displayLoader = false;
      });
  }

  /**
   * Function for Dashboard Performance part
   */
  performanceGraph(type, month, year) {
    this.displayLoader = true;
    this.performanceRadioModel = type;
    /** Select Model is Year means then Set Month Default as 12 */
    if (type === 'y') {
      month = 12;
    }

    /* Performance Line Chart */
    this.targetValueLineChart = [];
    this.actualValueLineChart = [];
    this.performanceLineChartLabels = [];

    /* Performance Bar Chart */
    this.performanceBarChartData = [{ data: [], label: '' }, { data: [], label: '' }];
    this.performanceBarChartLabels = [];

    /* Performance Doughnut Chart */
    this.performanceDoughnutChartDataBottom = [];

    if (typeof month !== 'number') {
      month = this.monthList.indexOf(month);
    }
    const paramData = {
      'selectType': type,
      'selectMonth': month,
      'selectYear': year
    };
    this.dashboardService.performanceGraph(paramData).subscribe(
      data => {
        this.displayLoader = false;
        this.targetValueLineChart = [];
        this.actualValueLineChart = [];
        this.performanceBarChartData = [{ data: [], label: '' }, { data: [], label: '' }];
        this.performanceBarChartLabels = [];
        this.performanceLineChartLabels = [];

        if (data['status'] === 'Success') {
          if (type !== 'm') {
            if (type === 'y') {
              /** LineChart */
              data['data'].forEach(element => {
                element.targetValue = (element.targetValue != null) ? Math.trunc(element.targetValue) : 100000000;
                element.actualValue = (element.actualValue != null) ? Math.trunc(element.actualValue) : 100000000;
                this.targetValueLineChart.push(element.targetValue);
                this.actualValueLineChart.push(element.actualValue);
                this.performanceLineChartLabels.push(this.monthList[element.productionMonth]);
              });
            } else {
              /** BarChart */
              // Assigh Month Based on Quaters
              const totalMonth = [[1, 2, 3], [4, 5, 6], [7, 8, 9], [10, 11, 12]];
              const monthIndex = this.getIndexOfK(month, totalMonth);  // Get Quater Variable Index Value of Selected Month
              let selectedQuater = [];
              selectedQuater = totalMonth[monthIndex];
              for (let j = 0; j < selectedQuater.length; j++) {
                selectedQuater[j] = selectedQuater[j] - 1;  // Set previous value of array for Array Index
              }
              let k = 0;
              data['data'].forEach(element => {
                if (selectedQuater.indexOf(k) >= 0) {
                  element.targetValue = (element.targetValue != null) ? Math.trunc(element.targetValue) : 100000000;
                  element.actualValue = (element.actualValue != null) ? Math.trunc(element.actualValue) : 100000000;
                  this.performanceBarChartLabels.push(this.monthList[element.productionMonth]);
                  this.performanceBarChartData[0].data.push(element.targetValue);
                  this.performanceBarChartData[1].data.push(element.actualValue);
                }
                k++;
              });
              this.performanceBarChartData[0].label = 'Target Value';
              this.performanceBarChartData[1].label = 'Actual Value';
              console.log(this.performanceBarChartLabels);
              console.log(this.performanceBarChartData);
            }
          } else {
            /** DoughnutChart */
            this.performanceDoughnutChartDataBottom = [];
            const totalSize = data['data'].length;
            let targetValue = 0;
            let actualValue = 0;
            if (month <= totalSize) {
              // tslint:disable-next-line:max-line-length
              if (data['data'][totalSize - 1] !== null && data['data'][totalSize - 1] !== undefined) {
                targetValue = (data['data'][totalSize - 1]['targetValue'] != null) ?
                  Math.trunc(data['data'][totalSize - 1]['targetValue']) : 1000000000;
              }
              // tslint:disable-next-line:max-line-length
              if (data['data'][totalSize - 1] !== null && data['data'][totalSize - 1] !== undefined) {
                actualValue = (data['data'][totalSize - 1]['actualValue'] != null) ?
                  Math.trunc(data['data'][totalSize - 1]['actualValue']) : 1000000000;
              }
            }
            this.performanceDoughnutChartDataBottom.push(targetValue);
            this.performanceDoughnutChartDataBottom.push(actualValue);
            console.log("Target Bottom Value");
            console.log(this.performanceDoughnutChartDataBottom);
          }

          this.performanceLineChart = [
            {
              data: this.targetValueLineChart,
              label: 'Target'
            },
            {
              data: this.actualValueLineChart,
              label: 'Actual'
            }
          ];
        }
      }, error => {
        this.displayLoader = false;
      });

  }


  /**
   * Function for Dashboard team Performance part
   */
  teamPerformanceGraph(month, year) {
    this.displayLoader = true;
    if (typeof month !== 'number') {
      month = this.monthList.indexOf(month);
    }
    const paramData = {
      'periodMonthFrom': month,
      'periodYearFrom': year
    };
    this.dashboardService.teamPerformanceGraph(paramData).subscribe(
      data => {
        this.displayLoader = false;
        if (data['status'] === 'Success') {
          this.teamPerformanceResultData = data['data'];
          if (data['data'].length > 0) {
            data['data'].forEach(element => {
              element['symbol'] = (element['actualValue'] > element['previousResultValue']) ? 'up' : 'down';
              element['percentage'] = ((element['actualValue'] - element['previousResultValue']) /
                (element['previousResultValue']) * 100);
              if (element['previousResultValue'] !== null) {
                element['percentage'] = element['percentage'].toFixed(2);
              } else {
                element['percentage'] = '100.00';
              }
              // element['joiningDate'] = this.datePipe.transform(element['joiningDate'], 'dd-MM-yyyy');
            });
          }
        }
      }, error => {
        this.displayLoader = false;
      });
  }


  /**
   * Function for Dashboard Performance Production part
   */
  performanceProductionGraph(month, year) {
    this.displayLoader = true;
    if (typeof month !== 'number') {
      month = this.monthList.indexOf(month);
    }
    const paramData = {
      'periodMonthFrom': month,
      'periodYearFrom': year
    };
    this.dashboardService.performanceProductionGraph(paramData).subscribe(
      data => {
        this.displayLoader = false;
        let mtdActualValue = 0;
        let mtdPrevioValue = 0;
        let qtdActualValue = 0;
        let qtdPrevioValue = 0;
        let ytdActualValue = 0;
        let ytdPrevioValue = 0;
        console.log("performanceProductionGraph Response ");
        console.log(data);
        if (data['status'] === 'Success') {
          this.performanceProductionBarChartData = [{ data: [], label: '' }, { data: [], label: '' }];
          this.performanceProductionBarChartLabels = [];
          if (data['data'].length > 0) {
            data['data'].forEach(element => {
              mtdActualValue += element.mtdActualValue;
              mtdPrevioValue += element.mtdPrevioValue;
              qtdActualValue += element.qtdActualValue;
              qtdPrevioValue += element.qtdPrevioValue;
              ytdActualValue += element.ytdActualValue;
              ytdPrevioValue += element.ytdPrevioValue;
            });
            this.performanceProductionBarChartLabels = ['MTD', 'QTD', 'YTD'];
            this.performanceProductionBarChartData[0].data.push(mtdActualValue);
            this.performanceProductionBarChartData[1].data.push(mtdPrevioValue);
            this.performanceProductionBarChartData[0].data.push(qtdActualValue);
            this.performanceProductionBarChartData[1].data.push(qtdPrevioValue);
            this.performanceProductionBarChartData[0].data.push(ytdActualValue);
            this.performanceProductionBarChartData[1].data.push(ytdPrevioValue);
            this.performanceProductionBarChartData[0].label = 'Current';
            this.performanceProductionBarChartData[1].label = 'Previous';
            console.log("AFter Code Write");
            console.log(this.performanceProductionBarChartData);
            console.log(this.performanceProductionBarChartLabels);
          }
        }
      }, error => {
        this.displayLoader = false;
      });
  }

  /**
   * Function for Dashboard Contest Details part
   */
  contestDetails() {
    this.displayLoader = true;
    this.dashboardService.contestDetails().subscribe(
      data => {
        this.displayLoader = false;
        let totalLength = 0;
        let totalPercentage = 0;
        if (data['status'] === 'Success') {
          this.contestDetailEvaluationDoughnutChartData = [];
          totalLength = data['data'].length;
          if (data['data'].length > 0) {
            data['data'].forEach(element => {
              const calc = element['targetTo'] - element['targetFrom'];
              if (calc > 0) {
                totalPercentage += (((element['targetTo'] - element['targetFrom']) /
                  element['achievedValue']) * 100);
              } else {
                totalPercentage += 100;
              }
            });
            const evaluationPercentage = totalPercentage / totalLength;
            this.contestDetailEvaluationDoughnutChartData.push((100 - evaluationPercentage));
            this.contestDetailEvaluationDoughnutChartData.push(evaluationPercentage);
          }
        }
      }, error => {
        this.displayLoader = false;
      });
  }


  /**
   * Function for Dashboard Agent Application Status Part
   */
  agentApplicationStatus() {
    this.displayLoader = true;
    this.dashboardService.agentApplicationStatus().subscribe(
      data => {
        this.displayLoader = false;
        // this.agentApplicationLabels = [];
        this.agentApplicationData = [];
        if (data['status'] === 'Success') {
          if (data['data'].length > 0) {
            data['data'].forEach(element => {
              this.agentApplicationData.push(element.totalApplicants);
              this.agentApplicationLabels.push(element.applicationStatus);
            });
          }
          console.log("After Response HERE LOREM"); 
          console.log(this.agentApplicationLabels);
    console.log(this.agentApplicationData);
        }
      }, error => {
        this.displayLoader = false;
      });
  }


  getIndexOfK(k, arr) {
    for (let i = 0; i < arr.length; i++) {
      const index = arr[i].indexOf(k);
      if (index > -1) {
        return i;
      }
    }
  }

}
